#include "Renderer.h"
#include<iostream>
//a simple function to check the which line error happens I learned this from https://youtu.be/W3gAzLwfIP0 cherno tutorials
void GLClearError()
{
	while (glGetError != GL_NO_ERROR);
}

bool GLLogCall(const char * function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error] (" << error << ")" << function << " " << file << " : " << line << std::endl;
		return false;
	}
	return true;
}

//Mine
void Renderer::Draw(unsigned int va, const IndexBuffer& ib, unsigned int shader) const
{
	glUseProgram(shader);

	glBindVertexArray(va);

	ib.Bind();
	glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr);
}
//Mine
void Renderer::Clear() const
{
	glClear(GL_COLOR_BUFFER_BIT);
}