#include "VertexBuffer.h"
#include "Renderer.h"
//mine
VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
	glGenBuffers(1, &m_RendererID);

	//we want to interpret that buffer as an array
	glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
	//put data to this buffer with the sizeof our forexameple triangle data
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &m_RendererID);
}

void VertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
}

void VertexBuffer::UnBind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}