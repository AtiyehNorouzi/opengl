#pragma once
#include <GL/glew.h>
#include "IndexBuffer.h"
//I learned this from https://youtu.be/W3gAzLwfIP0 cherno tutorials
//for debugging for use it just put the line in GLCall like GLCall(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0));
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
x;\
ASSERT(GLLogCall(#x, __FILE__, __LINE__))
void GLClearError();


bool GLLogCall(const char * function, const char* file, int line);

class Renderer
{
public:
	void Clear() const;
	void Draw(unsigned int va, const IndexBuffer& ib, unsigned int shader) const;

};
