#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
//the core loop is from https://www.glfw.org/documentation.html and have to be same for anything you want to draw on screen even an empty window

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

//I watched a tutorial on how to write shaders in seprate file instead of having string in main class I learned this from https://youtu.be/W3gAzLwfIP0 cherno tutorials
static ShaderProgramSource ParseShader(const std::string & filepath)
{
	std::ifstream stream(filepath);

	enum class ShaderType
	{
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};
	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;
	while (getline(stream, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
				type = ShaderType::VERTEX;
			else if(line.find("fragment") != std::string::npos)
				type = ShaderType::FRAGMENT;
		}
		else
		{
			ss[(int)type] << line << '\n';
		}

	}
	return { ss[0].str() , ss[1].str() };
}

//Mine, a simple function provide it a shader type like vertex,fragment, tesselation , etc and bind it to an id with the source text you give to it as a parameter
static unsigned int CompileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	//memory address of first character in source
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::cout << " Failed to compile" << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << "shader!" << std::endl;
		std::cout << message << std::endl;

		glDeleteShader(id);
		return 0;
	}
	return id;
}
//Mine
// we want to combine these and get a shader id back from them
static unsigned int CreateShader(const std::string & vertexShader, const std::string& fragmentShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	//you can detach your linked shaders to clean the memory
	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	//Mine, change our openGL version to 3.3 from latest version and in core profile its mandatory to use vaos
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/*from glfw doc Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	//mine to slow the animation timelapses
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
		std::cout << "Error!" << std::endl;

	//Mine : Start and anywhere, must be same to draw a simple shape (triangle) on the screen I also used indices to remove the redundant indices
	std::cout << glGetString(GL_VERSION);
	float positions[] = {
			-0.5f, -0.5f, //0
			 0.5f, -0.5f, //1
			 0.5f, 0.5f,  //2
			- 0.5f, 0.5f  //3
	};

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};
	//Mine : end and anywhere, must be same to draw a simple shape (triangle) on the screen

	// Mine vertex attribute object which also contains the vertex buffer and it is needed in core_profile version of open gl which is a container of buffers with an id
	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// from  https://youtu.be/W3gAzLwfIP0 cherno tutorials
	//define a vertex buffer here we have one buffer and buffer is the returned id for that buffer
	VertexBuffer vb(positions, 4 * 2 * sizeof(float));
	glEnableVertexAttribArray(0);

	// from  https://youtu.be/W3gAzLwfIP0 cherno tutorials
	//the forth parameter is how far you have to go to reach the second position
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);

	IndexBuffer ib(indices, 6);
	//Mine, accesing our shader by passing the filepath to it
	ShaderProgramSource source = ParseShader("Basic.shader");

	//Mine,
	unsigned int shader = CreateShader(source.VertexSource, source.FragmentSource);
	glUseProgram(shader);

	Renderer renderer;

	//Mine, changing the r variable of rgb by changing is variable in main function
	float r = 0.0f;
	//Mine, changing the r variable by Increment in each frame
	float increment = 0.05f;


	// from  https://youtu.be/W3gAzLwfIP0 cherno tutorials
	glBindVertexArray(0);
	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		renderer.Clear();

		/* Mine accessing the u_color attribute of fragment shader from here */
		glUniform4f(glGetUniformLocation(shader, "u_Color"), r, 0.3f, 0.8f, 1.0f);

		renderer.Draw(vao, ib, shader);
		
		/* Mine if the r is above 1 then decrease it else increase it by 0.05 each frame*/
		if (r > 1.0f)
			increment = -0.05f;
		else if (r < 0.0f)
			increment = 0.05f;
		r += increment;


		/* from glfw doc Swap front and back buffers */
		glfwSwapBuffers(window);

		/* from glfw doc Poll for and process events */
		glfwPollEvents();
	}
	//glDeleteShader(shader);
	glfwTerminate();
	return 0;
}






























//
//
//#include <GL/glew.h>
//#include <GLFW/glfw3.h>
//#include<iostream>
//#include <fstream>
//#include <string>
//#include <sstream>
//
////for debugging for use it just put the line in GLCall like GLCall(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0));
//#define ASSERT(x) if (!(x)) __debugbreak();
//#define GLCall(x) GLClearError();\
//x;\
//ASSERT(GLLogCall(#x, __FILE__, __LINE__))
//static void GLClearError()
//{
//	while (glGetError != GL_NO_ERROR);
//}
//
//static bool GLLogCall(const char * function, const char* file, int line)
//{
//	while (GLenum error = glGetError())
//	{
//		std::cout << "[OpenGL Error] (" << error << ")" << function << " " << file << " : " << line << std::endl;
//		return false;
//	}
//	return true;
//}
//struct ShaderProgramSource
//{
//	std::string VertexSource;
//	std::string FragmentSource;
//};
//
//static ShaderProgramSource ParseShader(const std::string & filepath)
//{
//	std::ifstream stream(filepath);
//
//	enum class ShaderType
//	{
//		NONE = -1, VERTEX = 0, FRAGMENT = 1
//	};
//	std::string line;
//	std::stringstream ss[2];
//	ShaderType type = ShaderType::NONE;
//	while (getline(stream, line))
//	{
//		if (line.find("#shader") != std::string::npos)
//		{
//			if (line.find("vertex") != std::string::npos)
//				type = ShaderType::VERTEX;
//			else if (line.find("fragment") != std::string::npos)
//				type = ShaderType::FRAGMENT;
//		}
//		else
//		{
//			ss[(int)type] << line << '\n';
//		}
//
//	}
//	return { ss[0].str() , ss[1].str() };
//}
//static unsigned int CompileShader(unsigned int type, const std::string& source)
//{
//	unsigned int id = glCreateShader(type);
//	//memory address of first character in source
//	const char* src = source.c_str();
//	glShaderSource(id, 1, &src, nullptr);
//	glCompileShader(id);
//
//	int result;
//	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
//	if (result == GL_FALSE)
//	{
//		int length;
//		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
//		char* message = (char*)alloca(length * sizeof(char));
//		glGetShaderInfoLog(id, length, &length, message);
//		std::cout << " Failed to compile" << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << "shader!" << std::endl;
//		std::cout << message << std::endl;
//
//		glDeleteShader(id);
//		return 0;
//	}
//	return id;
//}
//
//// we want to combine these and get a shader id back from them
//static unsigned int CreateShader(const std::string & vertexShader, const std::string& fragmentShader)
//{
//	unsigned int program = glCreateProgram();
//	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
//	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);
//	glAttachShader(program, vs);
//	glAttachShader(program, fs);
//	glLinkProgram(program);
//	glValidateProgram(program);
//
//	//you can detach your linked shaders to clean the memory
//	glDeleteShader(vs);
//	glDeleteShader(fs);
//
//	return program;
//}
//
//int main(void)
//{
//	GLFWwindow* window;
//
//	/* Initialize the library */
//	if (!glfwInit())
//		return -1;
//
//	//change our openGL version to 3.3 from latest version
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
//
//	/* Create a windowed mode window and its OpenGL context */
//	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
//	if (!window)
//	{
//		glfwTerminate();
//		return -1;
//	}
//
//	/* Make the window's context current */
//	glfwMakeContextCurrent(window);
//
//	glfwSwapInterval(1);
//
//	if (glewInit() != GLEW_OK)
//		std::cout << "Error!" << std::endl;
//
//	std::cout << glGetString(GL_VERSION);
//	float positions[] = {
//		-0.5f, -0.5f, //0
//		0.5f, -0.5f, //1
//		0.5f, 0.5f,  //2
//		-0.5f, 0.5f  //3
//	};
//
//	unsigned int indices[] = {
//		0, 1, 2,
//		2, 3, 0
//	};
//
//	unsigned int vao;
//	glGenVertexArrays(1, &vao);
//	glBindVertexArray(vao);
//
//	//define a vertex buffer here we have one buffer and buffer is the returned id for that buffer
//	unsigned int buffer;
//	glGenBuffers(1, &buffer);
//
//	//we want to interpret that buffer as an array
//	glBindBuffer(GL_ARRAY_BUFFER, buffer);
//	//put data to this buffer with the sizeof our forexameple triangle data
//	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(float), positions, GL_STATIC_DRAW);
//
//	glEnableVertexAttribArray(0);
//
//	//the forth parameter is how far you have to go to reach the second position
//	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);
//
//	unsigned int ibo;
//	glGenBuffers(1, &ibo);
//
//	//we want to interpret that buffer as an array
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
//	//put data to this buffer with the sizeof our forexameple triangle data
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);
//
//
//	ShaderProgramSource source = ParseShader("Basic.shader");
//
//	unsigned int shader = CreateShader(source.VertexSource, source.FragmentSource);
//	glUseProgram(shader);
//	float r = 0.0f;
//	float increment = 0.05f;
//	//retrieve the location of variable
//	int location = glGetUniformLocation(shader, "u_Color");
//
//	/* Loop until the user closes the window */
//	while (!glfwWindowShouldClose(window))
//	{
//		/* Render here */
//		glClear(GL_COLOR_BUFFER_BIT);
//		glUniform4f(location, r, 0.3f, 0.8f, 1.0f);
//		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
//
//		if (r > 1.0f)
//			increment = -0.05f;
//		else if (r < 0.0f)
//			increment = 0.05f;
//		r += increment;
//
//		/* Swap front and back buffers */
//		glfwSwapBuffers(window);
//
//		/* Poll for and process events */
//		glfwPollEvents();
//	}
//	//glDeleteShader(shader);
//	glfwTerminate();
//	return 0;
//}